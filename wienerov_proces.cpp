#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <fstream>

using namespace std;

float norm_rozdelenie (float mi, float sigma)
{
	float x1, x2, y1, y2, w;
	bool b = 0;
	
	if (b)
	{
		y1 = y2;
		b = 0;
	}
	
	else
	{
		do
		{
			x1 = 2. * rand() / RAND_MAX - 1.;
			x2 = 2. * rand() / RAND_MAX - 1.;
			
			w = x1 * x1 + x2 * x2;
		}
		while (w >= 1.);
		
		w = sqrt((-2. * log(w)) / w);
		y1 = x1 * w;
		y2 = x2 * w;
		b = 1;
	}
	
	return (mi + y1 * sigma);	
}

int main()
{
	srand(time(NULL));
	
	int pocet = 10000, cas = 11;
	float t[pocet][cas], poc = 0;
	
	fstream subor1, subor2, subor3, subor4, subor5;
	subor1.open("trajektorie.dat", fstream::out);
	subor2.open("splnajuce_traj.dat", fstream::out);
	subor3.open("vektor.dat", fstream::out);
	subor4.open("inkrementy.dat", fstream::out);
	subor5.open("splnajuce_ink.dat", fstream::out);
	
	for (int i = 0; i < pocet; i++)
		t[i][0] = 0;
	
	for (int i = 0; i < pocet; i++)
	{
		for (int j = 1; j < cas; j++)
			t[i][j] = t[i][j - 1] + norm_rozdelenie(0,1);		 		
	}
	
	for (int i = 0; i < pocet; i++)
	{		
		if ((t[i][1] > 0) && ((t[i][2]) <= (2 * t[i][1]))) 
		{
			for (int j = 0; j < cas; j++) 
				subor2<<j<<" "<<t[i][j]<<endl;
		
			subor2<<endl;
			subor5<<t[i][1]<<" "<<t[i][2]-t[i][1]<<endl;
			
			subor3<<"1 "<<t[i][1]<<"\n2 "<<t[i][2]<<endl;
			
			poc++;	
		}	
		
	}

	for (int i = 0; i < pocet; i++)
	{		
		for (int j = 0; j < cas; j++) 
			subor1<<j<<" "<<t[i][j]<<endl;
		subor1<<endl;	
		subor4<<t[i][1]<<" "<<t[i][2]-t[i][1]<<endl;
	}
	

	cout<<"Pravdepodobnost javu je "<<poc/pocet<<endl;
	
	
	subor1.close();
	subor2.close();
	subor3.close();
	subor4.close();
	subor5.close();
	
	return 0;
}



